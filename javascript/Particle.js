class Particle{
    particle = {};
    maxSize = size;
    alpha = 1;
    constructor(size,destination){
      let radians = (Math.random() * 2)-1;
      this.x = Math.floor(Math.random() * canvas.width);
      this.y = Math.floor(Math.random() * canvas.height);
      this.radius = 0;
      this.lineSize = this.radius/2;
      this.velocity = (Math.random()*.5)+.1
      this.radians = radians;
    }
  
    move() {
      this.x += Math.cos(this.radians*Math.PI)*this.velocity;
      this.y += Math.sin(this.radians*Math.PI)*this.velocity;
      if(this.radius<this.maxSize){
        this.radius += .05;
        this.lineSize = this.radius;
      }
      //check for out of bounds
      if(this.x-this.radius<=0||this.x+this.radius>=canvas.width){
        this.radians += 1;
        this.radians = -this.radians;
      }
      if(this.y-this.radius<=0||this.y+this.radius>=canvas.height){
        this.radians = -this.radians;
      }
    }
    
    draw(){
      ctx.beginPath();
      ctx.globalAlpha = this.alpha;
      ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
      ctx.fillStyle = particleColor;
      ctx.fill();
      ctx.globalAlpha = 1;
    }
  
    drawLines(particles){
      //Draw line to mouse
      ctx.lineWidth = this.lineSize;
      if(mouse.valid){
        let dist = Math.sqrt(Math.pow(this.x - mouse.x,2)+Math.pow(this.y - mouse.y,2))
        if(dist<149){
          ctx.beginPath();
          ctx.moveTo(this.x, this.y);
          ctx.strokeStyle = lineColor;
          ctx.globalAlpha = Math.min(1-dist/150,.3);
          ctx.lineTo(mouse.x, mouse.y);
          ctx.stroke();
        }
        ctx.globalAlpha = 1;
      }
      
      //Draw Lines to neighbor particles
      particles.forEach((particle)=>{
        let dist = Math.sqrt(Math.pow(this.x -particle.x,2)+Math.pow(this.y -particle.y,2))
        if(dist>99) return;
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.strokeStyle = lineColor;
        ctx.lineWidth = Math.min(this.lineSize,particle.lineSize);
        ctx.globalAlpha = Math.min(1-dist/101,.3);
        ctx.lineTo(particle.x, particle.y);
        ctx.stroke();
      });
      ctx.globalAlpha = 1;
    }
  }
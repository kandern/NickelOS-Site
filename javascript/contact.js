function validate(){
  let emailTest = new RegExp(".+@.+\\..+");
  let emailInput = document.getElementById("email");
  let email = emailInput.value;
    //if the email is invalid, don't submit
    if(!emailTest.test(email)){
      emailInput.setCustomValidity('Please Enter valid email')
      return false;
    }
    return true;
}
var mouse = {
    x: 0,
    y: 0,
    valid: false
};
  
var size = 2;
var coverage  = 1.3;
var numParticles;
var styles = getComputedStyle(document.documentElement);
var particleColor = "black";
var lineColor     = "rgb("+styles.getPropertyValue('--accent')+")";
var canvas = document.getElementById("particles");
var div = document.getElementById("personal-info");
if(div==null){
  div = document.getElementById("resume");
}
if(div==null){
  div = document.getElementById("contact");
}
var nav = document.getElementById("nav");
var ctx = canvas.getContext("2d");
ctx.translate(0.5, 0.5);
var particles = [];
function onResize(){
  var canvas_height = +getComputedStyle(canvas).getPropertyValue("height").slice(0, -2);
  var canvas_width = +getComputedStyle(canvas).getPropertyValue("width").slice(0, -2);
  canvas.width = canvas_width;
  canvas.height = canvas_height;
  canvas.style.width = "100%";
  canvas.style.height = "100%";
  ctx.translate(0.5, 0.5);
  populate(canvas_width,canvas_height);
}

function populate(canvas_width,canvas_height){
  numParticles = Math.floor((canvas_height * canvas_width) / 10000 * coverage);
  if(particles.length<numParticles){
    //if we increased the size, increased the size, spawn some new particles
    for(i=particles.length;i<numParticles;i++){
      particles.push(new Particle(size));
    }
  }else if(particles.length>numParticles){
    //if we decreased the size, remove any off the screen, if we are still overpopulated, remove until we aren't
    for(i=0;i<particles.length;i++){
      //if we are outside of the range, remove ourselves
      if(particles[i].x<=0 || particles[i].x>=canvas.width
        ||particles[i].y<=0 || particles[i].y>=canvas.height){
        particles.splice(i,1);
      }
    }
    //maybe we didn't delete enough
    for(i=particles.length-1;i>numParticles;i--){
      particles = particles.slice(0,numParticles);
    }
    //maybe we deleted too many
    for(i=particles.length;i<numParticles;i++){
      particles.push(new Particle(size));
    }
  }
}

function draw(){
  //clear everything
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  //Draw Particles
  for(let i = 0;i<particles.length;i++){
    particles[i].drawLines(particles.slice(i+1));
    particles[i].draw();
    particles[i].move();
  } 
  requestAnimationFrame(draw);
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

//setup and run everything*********************************************************************************************
onResize();
draw();

window.requestAnimationFrame = window.requestAnimationFrame
    || window.webkitRequestAnimationFrame
    || window.mozRequestAnimationFrame;
window.addEventListener('resize', onResize);
canvas.addEventListener('mousemove', function(evt) {
  var mousePos = getMousePos(canvas, evt);
  mouse.x = mousePos.x;
  mouse.y = mousePos.y;
},false);
canvas.addEventListener('mouseenter', () => {
  mouse.valid = true;
});
canvas.addEventListener('mouseleave', () => {
  mouse.valid = false;
});

div.addEventListener('mousemove', function(evt) {
  let mousePos = getMousePos(div, evt);
  let style = window.getComputedStyle(div, null);
  let yOffset = parseFloat(style.getPropertyValue("top"));
  let xOffset = parseFloat(style.getPropertyValue("left"));
  mouse.x = mousePos.x+xOffset;
  mouse.y = mousePos.y+yOffset;
},false);
div.addEventListener('mouseenter', () => {
  mouse.valid = true;
});
nav = document.getElementById("nav");
nav.addEventListener('mousemove', function(evt) {
  var mousePos = getMousePos(nav, evt);
  mouse.x = mousePos.x;
  mouse.y = mousePos.y;
},false);
nav.addEventListener('mouseenter', () => {
  mouse.valid = true;
});
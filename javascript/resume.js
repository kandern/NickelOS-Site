let current = "";
let sections = ["about","experience","skills"];

function toggle(element){
  sections.forEach(section=>{
    let content = document.getElementById(section+"-content");
    if(section+"-content" != element){
      content.style.height = "0";
    }else{
      if(content.style.height == content.getAttribute("data-size")){
        content.style.height = "0";
      }else{
        content.style.height = content.getAttribute("data-size");
      }
    }
  });
}
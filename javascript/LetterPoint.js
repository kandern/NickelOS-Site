class LetterPoint extends Particle{
  //next point in the character
  next;
  destination = {};
  inPlace = false;
  alpha = 1;
  //Destination is an array of percents[x%,y%].
  //.5 would be in the middle of the respective direction
  constructor(destination){
    super();
    this.xPercent = destination[0];
    this.yPercent = destination[1];
    this.velocity = Math.random()+3;
    this.Color = this.Color
    //calculate angle towards destination
    this.velocity = 1;
  }

  move() {
    if(Math.abs(this.x-this.destination.x)<1
        &&Math.abs(this.y-this.destination.y)<1){
      this.inPlace = true;
    }else{
      //this.radians = Math.atan2((this.destination.y-this.y),(this.destination.x-this.x));
      this.x += Math.cos(this.radians)*this.velocity;
      this.y += Math.sin(this.radians)*this.velocity;
    }
  }

  drawLines(){
    if(this.inPlace){
      if(this.next && this.next.inPlace){
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.strokeStyle = lineColor;
        ctx.lineWidth = this.lineSize;
        ctx.lineTo(this.next.x, this.next.y);
        ctx.stroke();
      }
    }
  }

  //we get the new destination everytime
  resize(canvas_width,canvas_height){
    this.destination.x = Math.floor(canvas_width*this.xPercent);
    this.destination.y = Math.floor(canvas_height*this.yPercent);
    this.x = this.destination.x;
    this.y = this.destination.y;
    this.radius = Math.min(canvas_width,canvas_height)/200;
    this.lineSize = this.radius*2;
    this.radians = Math.atan2((this.destination.y-this.y),(this.destination.x-this.x));
    console.log(this.destination.x + "|" + this.destination.y + "||" + canvas_width + "|" + canvas_height);
  }
}